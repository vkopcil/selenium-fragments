package net.happytesting.selenium.fragments;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import net.happytesting.selenium.fragments.pagefactory.FragmentDecorator;

import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

public class PageFactory {
    private PageFactory() {
    };

    public static <T> T initElements(WebDriver driver, Class<T> pageClassToProxy) {
        return createFragment(driver, driver, pageClassToProxy);
    }
    
    public static <T> T createFragment(WebDriver driver, SearchContext context,
            Class<T> pageClassToProxy) {
        T fragment = instantiatePage(driver, pageClassToProxy);
        final ElementLocatorFactory factoryRef = new DefaultElementLocatorFactory(
                context);
        org.openqa.selenium.support.PageFactory.initElements(new FragmentDecorator(driver, factoryRef), fragment);
        return fragment;
    }

    private static <T> T instantiatePage(WebDriver driver,
            Class<T> pageClassToProxy) {
        try {
            try {
                Constructor<T> constructor = pageClassToProxy
                        .getConstructor(WebDriver.class);
                return constructor.newInstance(driver);
            } catch (NoSuchMethodException e) {
                return pageClassToProxy.newInstance();
            }
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }
}
