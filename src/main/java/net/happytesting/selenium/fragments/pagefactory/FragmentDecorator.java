package net.happytesting.selenium.fragments.pagefactory;

import java.lang.reflect.Field;
import java.util.List;

import net.happytesting.selenium.fragments.PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

public class FragmentDecorator extends DefaultFieldDecorator {
    private final WebDriver driver;

    public FragmentDecorator(WebDriver driver, ElementLocatorFactory factory) {
        super(factory);
        this.driver = driver;
    }

    public Object decorate(ClassLoader loader, Field field) {
        if (field.getType().isAssignableFrom(WebDriver.class) && field.getAnnotation(Driver.class) != null) {
          return driver;
        }
        Object webElementProxy = super.decorate(loader, field);
        if (webElementProxy != null) {
            return webElementProxy;
        }
        if (List.class.isAssignableFrom(field.getType())) {
            return null;
        }
        if (field.getAnnotation(FindBy.class) == null) {
          return null;
        }
        ElementLocator locator = factory.createLocator(field);
        if (locator == null) {
            return null;
        }
        return PageFactory.createFragment(driver,
                proxyForLocator(loader, locator), field.getType());
    }
}
