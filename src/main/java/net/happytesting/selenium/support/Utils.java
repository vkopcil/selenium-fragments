package net.happytesting.selenium.support;

import java.util.List;

import net.happytesting.selenium.fragments.PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

public class Utils {
  private Utils() {
  }

  public static <T> List<T> toFragments(final WebDriver driver,
      List<WebElement> items, final Class<T> fragmentClass) {
    return ImmutableList.copyOf(Iterables.transform(items,
        new Function<WebElement, T>() {

          public T apply(WebElement element) {
            return PageFactory.createFragment(driver, element, fragmentClass);
          }
        }));
  }

  public static <T, V extends View<? extends T>> List<T> fromView(
      WebDriver driver, List<WebElement> items, Class<V> viewClass) {
    /*
     * return ImmutableList.copyOf(Iterables.transform(items,
     * 
     * new Function<WebElement, T>() {
     * 
     * @Override
     * 
     * public T apply(WebElement item) {
     * 
     * return PageFactory.createFragment(driver, item, viewClass).get();
     * 
     * }
     * 
     * }));
     */

    return ImmutableList.copyOf(items.stream()
        .map(item -> PageFactory.createFragment(driver, item, viewClass).get())
        .iterator());
  }

  public static <T, V extends View<? extends T>> List<T> fromView(
      WebDriver driver, List<WebElement> items, TypeFactory<V> factory) {
    return ImmutableList.copyOf(items
        .stream()
        .map(
            item -> PageFactory.createFragment(driver, item,
                factory.getClass(item)).get()).iterator());
  }

  // section.setItems(ImmutableList.copyOf(links
  // .stream()
  // .map(
  // link -> PageFactory
  // .createFragment(driver, link, LinkFragment.class).get())
  // .iterator()));
  // List<MenuItem> links = new LinkedList<>();
  // section.setItems(links);
  // for (WebElement link : this.links) {
  // links.add(PageFactory.createFragment(driver, link, LinkFragment.class)
  // .get());
  // }

}
