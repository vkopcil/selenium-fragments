package net.happytesting.selenium.support;

import org.openqa.selenium.WebElement;

public interface TypeFactory<T> {
  Class<? extends T> getClass(WebElement element);
}
