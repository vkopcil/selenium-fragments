package net.happytesting.selenium.support;

public interface View<T> {
  T get();
}
